solution "deglutition"
	configurations { "Debug", "Release" }
	platforms { "x32", "x64" }
	location "build"

	project "EGL"
	kind "SharedLib"
	language "C"
	includedirs { "inc" }
	files { "inc/EGL/*.h", "src/GL/gl.c", "src/GLES/gles.c", "src/GLES2/gles2.c", "src/GLES3/gles3.c" }
	configuration "x32"
		targetdir "lib"
	configuration "x64"
		targetdir "lib64"
	configuration "windows"
		files { "src/WGL/egl_wgl.c" }
		links { "gdi32", "opengl32" }
	configuration "linux"
		files { "src/GLX/egl_glx.c" }
	configuration "Debug"
		defines { "_DEBUG" }
		flags { "Symbols", "ExtraWarnings" }
		targetsuffix "d"
	configuration "Release"
		defines { "NDEBUG" }
		flags { "Optimize" }

	project "GLcore"
	kind "SharedLib"
	language "C"
	includedirs { "inc" }
	files { "inc/GL/*.h", "src/GL/glstubs.c" }
	configuration "x32"
		targetdir "lib"
	configuration "x64"
		targetdir "lib64"
	configuration "windows"
		links { "EGL" }
	configuration "Debug"
		defines { "_DEBUG" }
		flags { "Symbols", "ExtraWarnings" }
		targetsuffix "d"
	configuration "Release"
		defines { "NDEBUG" }
		flags { "Optimize" }

	project "GLES"
	kind "SharedLib"
	language "C"
	includedirs { "inc" }
	files { "inc/GLES/*.h", "src/GLES/glesstubs.c" }
	configuration "x32"
		targetdir "lib"
	configuration "x64"
		targetdir "lib64"
	configuration "windows"
		links { "EGL" }
	configuration "Debug"
		defines { "_DEBUG" }
		flags { "Symbols", "ExtraWarnings" }
		targetsuffix "d"
	configuration "Release"
		defines { "NDEBUG" }
		flags { "Optimize" }

	project "GLESv2"
	kind "SharedLib"
	language "C"
	includedirs { "inc" }
	files { "inc/GLES2/*.h", "src/GLES2/gles2stubs.c" }
	configuration "x32"
		targetdir "lib"
	configuration "x64"
		targetdir "lib64"
	configuration "windows"
		links { "EGL" }
	configuration "Debug"
		defines { "_DEBUG" }
		flags { "Symbols", "ExtraWarnings" }
		targetsuffix "d"
	configuration "Release"
		defines { "NDEBUG" }
		flags { "Optimize" }

	project "GLESv3"
	kind "SharedLib"
	language "C"
	includedirs { "inc" }
	files { "inc/GLES3/*.h", "src/GLES3/gles3stubs.c" }
	configuration "x32"
		targetdir "lib"
	configuration "x64"
		targetdir "lib64"
	configuration "windows"
		links { "EGL" }
	configuration "Debug"
		defines { "_DEBUG" }
		flags { "Symbols", "ExtraWarnings" }
		targetsuffix "d"
	configuration "Release"
		defines { "NDEBUG" }
		flags { "Optimize" }

	project "eglinfo"
	kind "ConsoleApp"
	language "C"
	targetdir "bin"
	includedirs { "inc" }
	files { "src/utils/eglinfo.c" }
	links { "EGL" }
	configuration "windows"
		links { "gdi32", "opengl32" }
	configuration "linux"
		links { "X11", "GL" }
	configuration "Debug"
		defines { "_DEBUG" }
		flags { "Symbols", "ExtraWarnings" }
		targetsuffix "d"
	configuration "Release"
		defines { "NDEBUG" }
		flags { "Optimize" }
